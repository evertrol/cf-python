Installation of the ESMF package for regridding
===============================================

From version 1.0.4, the ESMF package
(https://www.earthsystemcog.org/projects/esmf) is may be used for
regridding. If this package is not installed then regridding will not
work, but all other cf-python functionality is unaffected.

### Download and unpack ESMF

##### cf-python versions 1.1.4 and newer 

ESMF version ESMF_7_0_0 must be used. Fill in the registration form at:

  http://www.earthsystemmodeling.org/esmf_releases/non_public/ESMF_7_0_0/reg/ESMF_Framework_Reg.html

and proceed to the download page. Once downloaded, unpack the source
code:

    tar zxvf esmf*.tar.gz

### Build ESMF (bash)

    cd esmf  
    export ESMF_DIR=$PWD
    make
    make all_tests

### Build ESMF (csh)

    cd esmf
    setenv ESMF_DIR=$PWD
    make
    make all_tests

### Python package installation as root

    cd $ESMF_DIR/src/addon/ESMPy
    ESMFMKFILE=$ESMF_DIR/lib/lib<g<or>O>/<platform>/esmf.mk
    python setup.py build --ESMFMKFILE=$ESMFMKFILE install

For example, ESMFMKFILE could be
`$ESMF_DIR/lib/libO/Linux.gfortran.64.mpiuni.default/esmf.mk`

### Python package installation as user

    cd $ESMF_DIR/src/addon/ESMPy
    ESMFMKFILE=$ESMF_DIR/lib/lib<g<or>O>/<platform>/esmf.mk
    python setup.py build --ESMFMKFILE=$ESMFMKFILE
    python setup.py install --prefix=<install_location>

For example, ESMFMKFILE could be
`$ESMF_DIR/lib/libO/Linux.gfortran.64.mpiuni.default/esmf.mk`

bash:

    export PYTHONPATH=<install_location>/lib/*/site_packages:$PYTHONPATH

or csh:

    setenv PYTHONPATH <install_location>/lib/*/site_packages
