.. currentmodule:: cf
.. default-role:: obj

cf.Datetime
===========

.. autoclass:: cf.Datetime
   :no-members:
   :no-inherited-members:

Methods
-------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Datetime.copy	
   ~cf.Datetime.inspect
   ~cf.Datetime.timetuple
   
Class methods
-------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.Datetime.utcnow
   
