.. currentmodule:: cf
.. default-role:: obj

cf.AuxiliaryCoordinate
======================

.. autoclass:: cf.AuxiliaryCoordinate
   :no-members:
   :no-inherited-members:

CF properties
-------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.add_offset
   ~cf.AuxiliaryCoordinate.axis
   ~cf.AuxiliaryCoordinate.calendar
   ~cf.AuxiliaryCoordinate.comment
   ~cf.AuxiliaryCoordinate._FillValue
   ~cf.AuxiliaryCoordinate.history
   ~cf.AuxiliaryCoordinate.leap_month
   ~cf.AuxiliaryCoordinate.leap_year
   ~cf.AuxiliaryCoordinate.long_name
   ~cf.AuxiliaryCoordinate.missing_value
   ~cf.AuxiliaryCoordinate.month_lengths
   ~cf.AuxiliaryCoordinate.positive
   ~cf.AuxiliaryCoordinate.scale_factor
   ~cf.AuxiliaryCoordinate.standard_name
   ~cf.AuxiliaryCoordinate.units
   ~cf.AuxiliaryCoordinate.valid_max
   ~cf.AuxiliaryCoordinate.valid_min
   ~cf.AuxiliaryCoordinate.valid_range


Attributes
----------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.AuxiliaryCoordinate.array
   ~cf.AuxiliaryCoordinate.binary_mask
   ~cf.AuxiliaryCoordinate.bounds
   ~cf.AuxiliaryCoordinate.cellsize
   ~cf.AuxiliaryCoordinate.ctype
   ~cf.AuxiliaryCoordinate.data
   ~cf.AuxiliaryCoordinate.day
   ~cf.AuxiliaryCoordinate.dtarray
   ~cf.AuxiliaryCoordinate.dtype
   ~cf.AuxiliaryCoordinate.hour
   ~cf.AuxiliaryCoordinate.hardmask
   ~cf.AuxiliaryCoordinate.hasbounds
   ~cf.AuxiliaryCoordinate.isauxiliary
   ~cf.AuxiliaryCoordinate.isdimension
   ~cf.AuxiliaryCoordinate.isscalar
   ~cf.DimensionCoordinate.lower_bounds
   ~cf.AuxiliaryCoordinate.mask
   ~cf.AuxiliaryCoordinate.minute
   ~cf.AuxiliaryCoordinate.month
   ~cf.AuxiliaryCoordinate.ndim
   ~cf.AuxiliaryCoordinate.second
   ~cf.AuxiliaryCoordinate.shape
   ~cf.AuxiliaryCoordinate.size
   ~cf.AuxiliaryCoordinate.subspace
   ~cf.AuxiliaryCoordinate.T
   ~cf.AuxiliaryCoordinate.unique 
   ~cf.AuxiliaryCoordinate.Units
   ~cf.DimensionCoordinate.upper_bounds
   ~cf.AuxiliaryCoordinate.varray
   ~cf.AuxiliaryCoordinate.X
   ~cf.AuxiliaryCoordinate.Y
   ~cf.AuxiliaryCoordinate.year
   ~cf.AuxiliaryCoordinate.Z

Methods
-------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.AuxiliaryCoordinate.asauxiliary
   ~cf.AuxiliaryCoordinate.asdimension
   ~cf.AuxiliaryCoordinate.attributes
   ~cf.AuxiliaryCoordinate.chunk
   ~cf.AuxiliaryCoordinate.clip
   ~cf.AuxiliaryCoordinate.close
   ~cf.AuxiliaryCoordinate.contiguous
   ~cf.AuxiliaryCoordinate.convert_reference_time
   ~cf.AuxiliaryCoordinate.copy
   ~cf.AuxiliaryCoordinate.cos
   ~cf.AuxiliaryCoordinate.datum
   ~cf.AuxiliaryCoordinate.delprop
   ~cf.AuxiliaryCoordinate.dump
   ~cf.AuxiliaryCoordinate.equals
   ~cf.AuxiliaryCoordinate.expand_dims
   ~cf.AuxiliaryCoordinate.files
   ~cf.AuxiliaryCoordinate.fill_value
   ~cf.AuxiliaryCoordinate.flip
   ~cf.AuxiliaryCoordinate.getprop
   ~cf.AuxiliaryCoordinate.hasprop
   ~cf.AuxiliaryCoordinate.HDF_chunks
   ~cf.AuxiliaryCoordinate.identity
   ~cf.AuxiliaryCoordinate.insert_bounds
   ~cf.AuxiliaryCoordinate.insert_data
   ~cf.AuxiliaryCoordinate.mask_invalid
   ~cf.AuxiliaryCoordinate.match
   ~cf.AuxiliaryCoordinate.name
   ~cf.AuxiliaryCoordinate.override_units
   ~cf.AuxiliaryCoordinate.properties
   ~cf.AuxiliaryCoordinate.select
   ~cf.AuxiliaryCoordinate.setprop
   ~cf.AuxiliaryCoordinate.sin
   ~cf.AuxiliaryCoordinate.squeeze
   ~cf.AuxiliaryCoordinate.transpose
   ~cf.AuxiliaryCoordinate.where
