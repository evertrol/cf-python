.. currentmodule:: cf
.. default-role:: obj

cf.Units
========

.. autoclass:: cf.Units
   :no-members:
   :no-inherited-members:

Attributes
----------

.. autosummary::
   :toctree: ../generated/	
   :template: attribute.rst

   ~cf.Units.calendar
   ~cf.Units.isdimensionless
   ~cf.Units.islatitude
   ~cf.Units.islongitude
   ~cf.Units.ispressure
   ~cf.Units.isreftime
   ~cf.Units.istime
   ~cf.Units.reftime
   ~cf.Units.units

Methods
-------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Units.conform
   ~cf.Units.copy
   ~cf.Units.dump
   ~cf.Units.equals
   ~cf.Units.equivalent
   ~cf.Units.formatted
   ~cf.Units.log

Static methods
--------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/	
   :template: method.rst

   ~cf.Units.conform
