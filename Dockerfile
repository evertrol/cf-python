FROM ubuntu:17.10
MAINTAINER Charles Roberts "c.j.roberts@reading.ac.uk"

# Run all ubuntu updates and default packages
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y git wget bzip2 build-essential python-dev gfortran && \
    apt-get clean 

# Get anaconda stuff
ENV anaconda_url https://repo.continuum.io/archive/Anaconda2-5.0.1-Linux-x86_64.sh
ENV anaconda_path /opt/anaconda
ENV PATH $anaconda_path/bin:$PATH

RUN wget -nv -O $downloads/anaconda.sh $anaconda_url && \
    /bin/bash $downloads/anaconda.sh -b -p $anaconda_path

RUN conda install -c ncas -c conda-forge cf-python cf-plot
RUN conda install -c conda-forge -c nesii netcdf-fortran=4.4.4 mpich esmpy
RUN rm $downloads/anaconda.sh && conda clean -yt

RUN conda info -a 
RUN ipython -c "print 'Hello World'"